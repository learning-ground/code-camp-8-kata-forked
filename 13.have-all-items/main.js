/**
 * @template T | object
 * @param { Array<T> } items
 * @param { Array<T> } keywords
 * @return { boolean }
 */
function haveAllItems_loop(items, keywords) {
  // Implement by for, while or do...while loop
  let i = 0
  while (i < keywords.length) {
    if (!items.includes(keywords[i++])) {
      return false
    }
  }
  return true
}

/**
 * @template T | object
 * @param { Array<T> } items
 * @param { Array<T> } keywords
 * @return { boolean }
 */
function haveAllItems_noLoop(items, keywords) {
  // Implement by Array.prototype.xxx() method instead loop
  return keywords.every((keyword) => {
    return items.some((item) => {
      if (typeof item === 'object') {
        return JSON.stringify(item) === JSON.stringify(keyword)
      }
      return item === keyword
    })
  })
}

module.exports = { haveAllItems_loop, haveAllItems_noLoop }
