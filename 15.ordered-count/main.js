/**
 * @param { string } string
 * @return { Array<[string, number]> }
 */
function findOrderedCount(string) {
  return Array.from(
    string.split('').reduce((acc, char) => {
      return acc.set(char, (acc.has(char)) ? acc.get(char) + 1 : 1)
    }, new Map())
  )
}

/* Array.prototype.forEach */
// function findOrderedCount(string) {
//   const chars = string.split('');
//   const map = new Map();
//   chars.forEach((char) => {
//     map.set(char, map.has(char) ? map.get(char) + 1 : 1)
//   })
//   return Array.from(map);
// }

module.exports = findOrderedCount
