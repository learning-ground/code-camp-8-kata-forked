/**
 * @param { Array<number> } numbers
 * @return { { min: number, max: number, avg: number } }
 */
function findMinMaxAndAvg(numbers) {
  const { max, min, avg } = doAll(numbers)
  return { max, min, avg }
}

function doAll(xs) {
  return xs.reduce(
    (acc, x, idx) => ({
      ...(idx === xs.length - 1 && { avg: Math.round((acc.sum + x) / xs.length) }),
      max: x > acc.max ? x : acc.max,
      min: x < acc.min ? x : acc.min,
      sum: acc.sum + x
    }),
    { max: -Infinity, min: Infinity, sum: 0 }
  )
}

module.exports = findMinMaxAndAvg
